#!/bin/bash

# Ingressar o Runner em modo shell no GitLab
# Se estiver utilizando gitlab.com ou certificado, altere para HTTPS://
sudo gitlab-runner register -n \
  --url https://gitlab.com/ \
  --registration-token GR13489416rmoqouSVdCYK_tpKmWM \
  --executor shell \
  --description "Runner Shell"

# Criar o container gitlab-runner para Docker
# Se estiver utilizando gitlab.com ou certificado, altere para HTTPS://
docker run -dit \
  --name runner-docker \
  --restart always \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v /opt/gitlab-runner/config:/etc/gitlab-runner \
  gitlab/gitlab-runner:ubuntu-v14.9.1

# Ingressar o Runner em modo docker no GitLab
# Se estiver utilizando gitlab.com ou certificado, altere para HTTPS://
docker exec -it runner-docker \
gitlab-runner register -n \
  --url "https://gitlab.com" \
  --registration-token "GR1348941UNuZK1a5xBpwy3DJAbG4" \
  --clone-url "https://gitlab.com" \
  --executor docker \
  --docker-image "docker:latest" \
  --docker-privileged

# docker run --rm -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register \
#   --url "https://gitlab.com/" \
#   --registration-token "GR1348941UNuZK1a5xBpwy3DJAbG4" \
#   --clone-url "https://gitlab.com" \
#   --token "$RUNNER_TOKEN" \
#   --executor "docker" \
#   --docker-image alpine:latest \
#   --description "docker-runner" \
#   --non-interactive



